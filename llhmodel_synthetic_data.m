function [ llh ] = llhmodel_synthetic_data( noise, stats )
%LLHMODEL Summary of this function goes here
%   Detailed explanation goes here

cn = noise(1);
sdn = noise(2);
pn = noise(3);

fprintf('Evaluating log likelihood for cn = %.5f, sdn = %.5f, pn = %.5f\n', cn, sdn, pn);
tic
[as, bs, phis] = model( cn, sdn, pn );
llh = - loglikelihood_synthetic_data(as, bs, phis, stats);
time_elapsed = toc/60;
fprintf('Time consumed %d minites\n', round(time_elapsed))


end

