function [ llh ] = llhmodel( noise, fig )
%LLHMODEL Summary of this function goes here
%   Detailed explanation goes here

% global viscosity;

if nargin == 0
    fprintf('debug mode in llhmodel.m\n')
    fig = figure;
    cn = 0.07;
    sdn = 0.19;
    pn = 0;
else
    cn = noise(1);
    sdn = noise(2);
    % pn = noise(3);
    pn = 0;
end

%% cost function optimization
% % fprintf('Evaluating log likelihood for cn = %.5f, sdn = %.5f, pn = %.5f\n', cn, sdn, pn);
% fprintf('Evaluating log likelihood for cn = %.5f, sdn = %.5f\n', cn, sdn);
% tic
% if debugMode
%     [~, ~, ~, stats] = model( cn, sdn, pn, fig);
% else
%     [~, ~, ~, stats] = model( cn, sdn, pn );
% end
% % llh = - loglikelihood(as, bs, phis);
% llh = costFunction(stats);
% time_elapsed = toc/60;
% fprintf('Time consumed %.1d minites, cost function %.4f\n', round(time_elapsed), llh)

%% maximum log likelihood estimation
if nargin == 0 || nargin == 2
    [as, bs, phis, ~] = model( cn, sdn, pn, fig);
else
    [as, bs, phis, ~] = model( cn, sdn, pn );
end
llh = - loglikelihood(as, bs, phis);
%% write result to file
% load('function estimation log.mat');
% vis = viscosity;
% k = vis * 10 + 1;
% fvalLog(k).viscosity = vis;
% i = round(cn * 100);
% j = round(sdn * 100);
% [a, b] = size(fvalLog(k).numMap);
% if i > a || j > b
%     fvalLog(k).numMap(i,j) = 1;
%     fvalLog(k).fvalMap(i,j) = llh;
% else
%     n = 1 + fvalLog(k).numMap(i,j);
%     fvalLog(k).numMap(i,j) = n;
%     fvalLog(k).fvalMap(i,j) = (llh + (n-1) * fvalLog(k).fvalMap(i,j)) / n;
% end
% save('function estimation log.mat', 'fvalLog');

end

