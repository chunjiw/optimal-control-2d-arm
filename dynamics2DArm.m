function [ statenext, u, Ak, ck, Bk ] = dynamics2DArm( state, u, motorNoise )
%DYNAMICS2DARM Summary of this function goes here
% last check 5/12/2015 -- Chunji Wang

global viscosity;

% if noise, corrupt motor command
if nargin > 2
    cn = motorNoise(1);
    sdn = motorNoise(2);
    u = (1 + sdn * randn(2,1)).*u + cn * randn(2,1);
end

dt = 0.01;
te = 0.03;
ta = 0.04;
L = [0.3,0.33];
I = [0.025 0.045];
m2 = 1;
d2 = 0.16;
l1 = L(1);
l2 = L(2);
a1 = sum(I) + m2*l1^2;
a2 = m2*l1*d2;
a3 = I(2);
B = viscosity * [1 0;0 1];
b11 = B(1,1);
b12 = B(1,2);
b21 = B(2,1);
b22 = B(2,2);

Ap = ...
    [1 0 dt 0   0       0       0       0       0       0       0       0
     0 1 0 dt   0       0       0       0       0       0       0       0
     0 0 0 0    1-dt/te 0       dt/te   0       0       0       0       0
     0 0 0 0    0       1-dt/te 0       dt/te   0       0       0       0
     0 0 0 0    0       0       1-dt/ta 0       0       0       0       0
     0 0 0 0    0       0       0       1-dt/ta 0       0       0       0];
Bp(5:6,:) = eye(2)*dt/ta;
AkBase([1,2,5:8],:) = Ap;
AkBase(9:12,9:12) = eye(4);
Bk(7:8,:) = eye(2)*dt/ta;
Bk(12,2) = 0;

theta2 = state(2);
theta1dot = state(3);
theta1dot2 = theta1dot^2;
theta2dot = state(4);
theta12dot = theta1dot+theta2dot;
ex1 = state(5);
ex2 = state(6);
a2s2 = a2*sin(theta2);
a2c2 = a2*cos(theta2);
a2a3s2 = a2s2*a3;
a2a3c2 = a2c2*a3;
a22s2c2 = a2s2*a2c2;
a22c22 = a2^2*cos(2*theta2);
bia22s2c2 = 2*a22s2c2;
a2s2ex2 = a2s2*ex2;
a1theta1dot2 = a1*theta1dot2;
S6 = a1*a3 - a3^2 - a2c2^2;
S62 = S6^2;
S7 = theta2dot * (2*theta1dot + theta2dot);
S8 = b21*theta1dot + b22*theta2dot;
S9 = b11*theta1dot + b12*theta2dot;
S10 = a3 + a2c2;
S11 = a2a3c2*S7;
S12 = a2a3s2*S7;
S13 = a22c22*theta1dot2;
S14 = a22s2c2*theta1dot2;
S15 = a1 + 2*a2c2;
S16 = dt/S62;
iM = [a3 -S10;-S10 S15]/S6;     % inverse of M
iMdt = dt*iM;
C = [-S7; theta1dot2]*a2s2;
Ak = AkBase;
Ak(3,2) = S16*(a2s2ex2*S6-(a3*ex1-S10*ex2)*bia22s2c2+(S11+a2a3c2*theta1dot2+S13)*S6-(S12+a2a3s2*theta1dot2+a22s2c2*theta1dot2)*bia22s2c2-a2s2*S8*S6+(a3*S9-S10*S8)*bia22s2c2);
Ak(4,2) = S16*((a2s2*ex1-2*a2s2ex2)*S6-(S15*ex2-S10*ex1)*bia22s2c2-(S11+a22c22*S7+a1theta1dot2*a2c2+2*S13)*S6+(S12+a22s2c2*S7+a1theta1dot2*a2s2+2*S14)*bia22s2c2-a2s2*(S9-2*S8)*S6+(S15*S8-S10*S9)*bia22s2c2);
Ak(3:4,3:4) = eye(2)-iMdt*(a2s2*2*[-theta2dot -theta12dot;theta1dot 0]+B);
Ak(3:4,5:6) = iMdt;
% the next state vector theta
statenext = state;
statenext([1,2,5:8]) = Ap*state + Bp*u;
statenext(3:4) = state(3:4) + iMdt*(state(5:6)-C-B*state(3:4));
% auxillary constant ck
if nargin < 3
    ck = statenext - Ak * state - Bk * u;
end

end

