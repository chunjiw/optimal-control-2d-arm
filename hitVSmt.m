function [ rate ] = hitVSmt( cn,sdn,T,direc,wr )
%hitVSmt Calculate the hit rate as a function of MT
%   Detailed explanation goes here

% sigma=0.03;
% sigmau=0.12;
% N=50;

d = 0.074;

para = struct('planningNoise', 0, 'motorNoise',[cn; sdn],...
         'N',1000,'T',T,'direc',direc,'d',d,'effortCost',wr);
[~, ~, ~, stat] = endpointstat(para);

tx = d * cos(direc);
ty = d * sin(direc);

hit = ((stat.endx - tx).^2 + (stat.endy - ty).^2) < 0.0001;  % target radius 1cm
rate = sum(hit) / 1000;

end