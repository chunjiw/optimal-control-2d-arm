pattern search with viscosity = 0.0, maximum likelihood estimation. result saved in pattern search result 3-1.mat

cn = 0.07500, sdn = 0.19512, log likelihood -0.40087  20-Jul-2015 22:56:31


Iter     f-count          f(x)      MeshSize     Method
    0        1       0.807587             1      
    1        4       0.471655             2     Successful Poll
    2        8       0.471655             1     Refine Mesh
    3       11       0.347351             2     Successful Poll
    4       14       0.347351             1     Refine Mesh
    5       18       0.347351           0.5     Refine Mesh
    6       20       0.340592             1     Successful Poll
    7       24       0.340592           0.5     Refine Mesh
    8       28        0.32117             1     Successful Poll
    9       32        0.32117           0.5     Refine Mesh
   10       36        0.32117          0.25     Refine Mesh
   11       40        0.32117         0.125     Refine Mesh
   12       44        0.32117        0.0625     Refine Mesh
   13       48       0.313331         0.125     Successful Poll
   14       52       0.313331        0.0625     Refine Mesh
   15       56       0.313331       0.03125     Refine Mesh
   16       60       0.313331       0.01563     Refine Mesh
   17       64       0.313331      0.007813     Refine Mesh
Optimization terminated: mesh size less than options.TolMesh.

Failed due to L drive network disconnection
Pattern search with viscosity = 0.1, maximum likelihood estimation. result saved in pattern search result 3-2.mat
cn = 0.20000, sdn = 0.20000, log likelihood -0.46079  21-Jul-2015 00:46:35
cn = 0.26250, sdn = 0.20000, log likelihood -0.71723  21-Jul-2015 00:50:20
cn = 0.20000, sdn = 0.26250, log likelihood -0.50954  21-Jul-2015 00:54:09
cn = 0.13750, sdn = 0.20000, log likelihood -0.23096  21-Jul-2015 00:57:57
cn = 0.26250, sdn = 0.20000, log likelihood -0.72243  21-Jul-2015 01:01:39
cn = 0.13750, sdn = 0.32500, log likelihood -0.39264  21-Jul-2015 01:05:28
cn = 0.01250, sdn = 0.20000, log likelihood -4.63685  21-Jul-2015 01:09:10
cn = 0.13750, sdn = 0.07500, log likelihood -0.45154  21-Jul-2015 01:12:58
cn = 0.20000, sdn = 0.20000, log likelihood -0.44872  21-Jul-2015 01:16:54
cn = 0.13750, sdn = 0.26250, log likelihood -0.27530  21-Jul-2015 01:20:44
cn = 0.07500, sdn = 0.20000, log likelihood -0.29067  21-Jul-2015 01:24:30
cn = 0.13750, sdn = 0.13750, log likelihood -0.24202  21-Jul-2015 01:29:24
cn = 0.16875, sdn = 0.20000, log likelihood -0.32436  21-Jul-2015 01:33:23
cn = 0.13750, sdn = 0.23125, log likelihood -0.26499  21-Jul-2015 01:37:23
cn = 0.10625, sdn = 0.20000, log likelihood -0.18110  21-Jul-2015 01:41:04
cn = 0.16875, sdn = 0.20000, log likelihood -0.31931  21-Jul-2015 01:44:41
cn = 0.10625, sdn = 0.26250, log likelihood -0.23720  21-Jul-2015 01:48:21
cn = 0.04375, sdn = 0.20000, log likelihood -1.01846  21-Jul-2015 01:51:59
cn = 0.10625, sdn = 0.13750, log likelihood -0.29329  21-Jul-2015 01:55:43
cn = 0.13750, sdn = 0.20000, log likelihood -0.20799  21-Jul-2015 01:59:40
cn = 0.10625, sdn = 0.23125, log likelihood -0.20123  21-Jul-2015 02:03:29
cn = 0.07500, sdn = 0.20000, log likelihood -0.29357  21-Jul-2015 02:07:07
cn = 0.10625, sdn = 0.16875, log likelihood -0.18617  21-Jul-2015 02:10:39
cn = 0.12188, sdn = 0.20000, log likelihood -0.18667  21-Jul-2015 02:14:17
cn = 0.10625, sdn = 0.21563, log likelihood -0.17363  21-Jul-2015 02:17:50
cn = 0.13750, sdn = 0.21563, log likelihood -0.23660  21-Jul-2015 02:21:29
cn = 0.10625, sdn = 0.24688, log likelihood -0.21617  21-Jul-2015 02:25:09



Pattern search with viscosity = 0.1, maximum likelihood estimation. result saved in pattern search result 3-2.mat

cn = 0.20000, sdn = 0.20000, log likelihood -0.45164  21-Jul-2015 11:02:37
cn = 0.26250, sdn = 0.20000, log likelihood -0.74022  21-Jul-2015 11:06:02
cn = 0.20000, sdn = 0.26250, log likelihood -0.52262  21-Jul-2015 11:09:24
cn = 0.13750, sdn = 0.20000, log likelihood -0.22123  21-Jul-2015 11:12:58
cn = 0.26250, sdn = 0.20000, log likelihood -0.73299  21-Jul-2015 11:16:41
cn = 0.13750, sdn = 0.32500, log likelihood -0.36495  21-Jul-2015 11:20:12
cn = 0.01250, sdn = 0.20000, log likelihood -4.42237  21-Jul-2015 11:23:40
cn = 0.13750, sdn = 0.07500, log likelihood -0.45764  21-Jul-2015 11:27:11
cn = 0.20000, sdn = 0.20000, log likelihood -0.45988  21-Jul-2015 11:30:54
cn = 0.13750, sdn = 0.26250, log likelihood -0.27075  21-Jul-2015 11:34:27
cn = 0.07500, sdn = 0.20000, log likelihood -0.32582  21-Jul-2015 11:38:06
cn = 0.13750, sdn = 0.13750, log likelihood -0.24539  21-Jul-2015 11:41:42
cn = 0.16875, sdn = 0.20000, log likelihood -0.32776  21-Jul-2015 11:45:21
cn = 0.13750, sdn = 0.23125, log likelihood -0.23322  21-Jul-2015 11:48:54
cn = 0.10625, sdn = 0.20000, log likelihood -0.18634  21-Jul-2015 11:52:23
cn = 0.16875, sdn = 0.20000, log likelihood -0.33253  21-Jul-2015 11:55:55
cn = 0.10625, sdn = 0.26250, log likelihood -0.22117  21-Jul-2015 11:59:19
cn = 0.04375, sdn = 0.20000, log likelihood -0.98910  21-Jul-2015 12:02:42
cn = 0.10625, sdn = 0.13750, log likelihood -0.24926  21-Jul-2015 12:06:10
cn = 0.13750, sdn = 0.20000, log likelihood -0.22066  21-Jul-2015 12:09:39
cn = 0.10625, sdn = 0.23125, log likelihood -0.19292  21-Jul-2015 12:13:08
cn = 0.07500, sdn = 0.20000, log likelihood -0.32010  21-Jul-2015 12:16:40
cn = 0.10625, sdn = 0.16875, log likelihood -0.18839  21-Jul-2015 12:20:08
cn = 0.12188, sdn = 0.20000, log likelihood -0.18754  21-Jul-2015 12:23:40
cn = 0.10625, sdn = 0.21563, log likelihood -0.17090  21-Jul-2015 12:27:09
cn = 0.13750, sdn = 0.21563, log likelihood -0.22165  21-Jul-2015 12:30:37
cn = 0.10625, sdn = 0.24688, log likelihood -0.21152  21-Jul-2015 12:34:08
cn = 0.07500, sdn = 0.21563, log likelihood -0.29386  21-Jul-2015 12:37:33
cn = 0.10625, sdn = 0.18438, log likelihood -0.18783  21-Jul-2015 12:41:07
cn = 0.12188, sdn = 0.21563, log likelihood -0.21747  21-Jul-2015 12:44:53
cn = 0.10625, sdn = 0.23125, log likelihood -0.19470  21-Jul-2015 12:48:42
cn = 0.09063, sdn = 0.21563, log likelihood -0.21057  21-Jul-2015 12:52:46
cn = 0.10625, sdn = 0.20000, log likelihood -0.18090  21-Jul-2015 12:56:23
cn = 0.11406, sdn = 0.21563, log likelihood -0.17525  21-Jul-2015 13:00:02
cn = 0.10625, sdn = 0.22344, log likelihood -0.18976  21-Jul-2015 13:03:41
cn = 0.09844, sdn = 0.21563, log likelihood -0.19835  21-Jul-2015 13:07:10
cn = 0.10625, sdn = 0.20781, log likelihood -0.17233  21-Jul-2015 13:10:40
cn = 0.11016, sdn = 0.21563, log likelihood -0.18984  21-Jul-2015 13:14:03
cn = 0.10625, sdn = 0.21953, log likelihood -0.18286  21-Jul-2015 13:17:34
cn = 0.10234, sdn = 0.21563, log likelihood -0.19034  21-Jul-2015 13:21:14
cn = 0.10625, sdn = 0.21172, log likelihood -0.19112  21-Jul-2015 13:24:46
cn = 0.10820, sdn = 0.21563, log likelihood -0.19672  21-Jul-2015 13:28:11
cn = 0.10625, sdn = 0.21758, log likelihood -0.17849  21-Jul-2015 13:31:41
cn = 0.10430, sdn = 0.21563, log likelihood -0.17497  21-Jul-2015 13:35:06
cn = 0.10625, sdn = 0.21367, log likelihood -0.18500  21-Jul-2015 13:38:46
cn = 0.10723, sdn = 0.21563, log likelihood -0.19070  21-Jul-2015 13:42:12
cn = 0.10625, sdn = 0.21660, log likelihood -0.17715  21-Jul-2015 13:45:45
cn = 0.10527, sdn = 0.21563, log likelihood -0.19206  21-Jul-2015 13:49:11
cn = 0.10625, sdn = 0.21465, log likelihood -0.17628  21-Jul-2015 13:53:04
optimization result cn = 0.10625, sdn = 0.21563 with viscosity = 0.1

Iter     f-count          f(x)      MeshSize     Method
    0        1       0.451639             1      
    1        4       0.221225             2     Successful Poll
    2        8       0.221225             1     Refine Mesh
    3       12       0.221225           0.5     Refine Mesh
    4       15       0.186344             1     Successful Poll
    5       19       0.186344           0.5     Refine Mesh
    6       23       0.186344          0.25     Refine Mesh
    7       25       0.170904           0.5     Successful Poll
    8       29       0.170904          0.25     Refine Mesh
    9       33       0.170904         0.125     Refine Mesh
   10       37       0.170904        0.0625     Refine Mesh
   11       41       0.170904       0.03125     Refine Mesh
   12       45       0.170904       0.01563     Refine Mesh
   13       49       0.170904      0.007813     Refine Mesh
Optimization terminated: mesh size less than options.TolMesh.







Pattern search with viscosity = 0.3, maximum likelihood estimation. result saved in pattern search result 3-4.mat
========>
cn = 0.20000, sdn = 0.20000, log likelihood -0.20054  21-Jul-2015 15:56:18
cn = 0.26250, sdn = 0.20000, log likelihood -0.30193  21-Jul-2015 15:59:24
cn = 0.20000, sdn = 0.26250, log likelihood -0.20921  21-Jul-2015 16:02:31
cn = 0.13750, sdn = 0.20000, log likelihood -0.24930  21-Jul-2015 16:05:46
cn = 0.20000, sdn = 0.13750, log likelihood -0.23475  21-Jul-2015 16:08:53
cn = 0.23125, sdn = 0.20000, log likelihood -0.21301  21-Jul-2015 16:12:02
cn = 0.20000, sdn = 0.23125, log likelihood -0.17739  21-Jul-2015 16:15:07
cn = 0.26250, sdn = 0.23125, log likelihood -0.33390  21-Jul-2015 16:18:13
cn = 0.20000, sdn = 0.29375, log likelihood -0.23494  21-Jul-2015 16:21:19
cn = 0.13750, sdn = 0.23125, log likelihood -0.19875  21-Jul-2015 16:24:29
cn = 0.20000, sdn = 0.16875, log likelihood -0.20104  21-Jul-2015 16:27:34
cn = 0.23125, sdn = 0.23125, log likelihood -0.23937  21-Jul-2015 16:30:44
cn = 0.20000, sdn = 0.26250, log likelihood -0.20306  21-Jul-2015 16:33:50
cn = 0.16875, sdn = 0.23125, log likelihood -0.17857  21-Jul-2015 16:36:56
cn = 0.20000, sdn = 0.20000, log likelihood -0.17861  21-Jul-2015 16:40:06
cn = 0.21563, sdn = 0.23125, log likelihood -0.21632  21-Jul-2015 16:43:13
cn = 0.20000, sdn = 0.24688, log likelihood -0.20192  21-Jul-2015 16:46:30
cn = 0.18438, sdn = 0.23125, log likelihood -0.16978  21-Jul-2015 16:49:49
cn = 0.21563, sdn = 0.23125, log likelihood -0.21086  21-Jul-2015 16:53:13
cn = 0.18438, sdn = 0.26250, log likelihood -0.18858  21-Jul-2015 16:56:33
cn = 0.15313, sdn = 0.23125, log likelihood -0.18903  21-Jul-2015 16:59:51
cn = 0.18438, sdn = 0.20000, log likelihood -0.15897  21-Jul-2015 17:03:04
cn = 0.24688, sdn = 0.20000, log likelihood -0.26710  21-Jul-2015 17:06:12
cn = 0.18438, sdn = 0.26250, log likelihood -0.18177  21-Jul-2015 17:09:32
cn = 0.12188, sdn = 0.20000, log likelihood -0.32700  21-Jul-2015 17:12:38
cn = 0.18438, sdn = 0.13750, log likelihood -0.21515  21-Jul-2015 17:15:44
cn = 0.21563, sdn = 0.20000, log likelihood -0.20794  21-Jul-2015 17:18:46
cn = 0.18438, sdn = 0.23125, log likelihood -0.16154  21-Jul-2015 17:21:54
cn = 0.15313, sdn = 0.20000, log likelihood -0.18957  21-Jul-2015 17:25:02
cn = 0.18438, sdn = 0.16875, log likelihood -0.18113  21-Jul-2015 17:28:13
cn = 0.20000, sdn = 0.20000, log likelihood -0.19286  21-Jul-2015 17:31:24
cn = 0.18438, sdn = 0.21563, log likelihood -0.18338  21-Jul-2015 17:34:30
cn = 0.16875, sdn = 0.20000, log likelihood -0.16479  21-Jul-2015 17:49:47
cn = 0.18438, sdn = 0.18438, log likelihood -0.18993  21-Jul-2015 17:53:00
cn = 0.19219, sdn = 0.20000, log likelihood -0.15996  21-Jul-2015 17:56:09
cn = 0.18438, sdn = 0.20781, log likelihood -0.17650  21-Jul-2015 17:59:14
cn = 0.17656, sdn = 0.20000, log likelihood -0.16795  21-Jul-2015 18:02:23
cn = 0.18438, sdn = 0.19219, log likelihood -0.17654  21-Jul-2015 18:05:35
cn = 0.18828, sdn = 0.20000, log likelihood -0.16556  21-Jul-2015 18:08:40
cn = 0.18438, sdn = 0.20391, log likelihood -0.16173  21-Jul-2015 18:11:44
cn = 0.18047, sdn = 0.20000, log likelihood -0.16883  21-Jul-2015 18:14:52
cn = 0.18438, sdn = 0.19609, log likelihood -0.17801  21-Jul-2015 18:18:08
cn = 0.18633, sdn = 0.20000, log likelihood -0.19517  21-Jul-2015 18:25:47
cn = 0.18438, sdn = 0.20195, log likelihood -0.16565  21-Jul-2015 18:29:06
cn = 0.18242, sdn = 0.20000, log likelihood -0.18255  21-Jul-2015 18:32:25
cn = 0.18438, sdn = 0.19805, log likelihood -0.17191  21-Jul-2015 18:35:33
cn = 0.18535, sdn = 0.20000, log likelihood -0.17233  21-Jul-2015 18:38:41
cn = 0.18438, sdn = 0.20098, log likelihood -0.17900  21-Jul-2015 18:41:47
cn = 0.18340, sdn = 0.20000, log likelihood -0.15983  21-Jul-2015 18:44:54
cn = 0.18438, sdn = 0.19902, log likelihood -0.16833  21-Jul-2015 18:48:04
<========
Optimization result cn = 0.18438, sdn = 0.20000 with viscosity = 0.3

Iter     f-count          f(x)      MeshSize     Method
    0        1       0.200545             1      
    1        5       0.200545           0.5     Refine Mesh
    2        7       0.177395             1     Successful Poll
    3       11       0.177395           0.5     Refine Mesh
    4       15       0.177395          0.25     Refine Mesh
    5       18       0.169783           0.5     Successful Poll
    6       22       0.158969             1     Successful Poll
    7       26       0.158969           0.5     Refine Mesh
    8       30       0.158969          0.25     Refine Mesh
    9       34       0.158969         0.125     Refine Mesh
   10       38       0.158969        0.0625     Refine Mesh
   11       42       0.158969       0.03125     Refine Mesh
   12       46       0.158969       0.01563     Refine Mesh
   13       50       0.158969      0.007813     Refine Mesh
Optimization terminated: mesh size less than options.TolMesh.