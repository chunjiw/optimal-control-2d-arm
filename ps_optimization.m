% solve the problem by pattern search

% cn, sdn, pn
% xstart = [0.2, 0.2, 0.05]';
% lb = [0.01; 0.01; 0.01];
% ub = [0.4; 0.4; 0.1];

% cn, sdn
clc
global viscosity;
global effortWeight;
xstart = [0.2, 0.2]';
lb = [.01; .01];
ub = [.4; .4];

diary('Round 8 Optimization Log.txt')
fprintf('Round 8 Optimization\n\n')
% fprintf('Round 6 Optimization - Function estimation log saved in matrix form\n\n')
% load('function estimation log.mat');
% create new log
% fvalLog.viscosity = 0;
% fvalLog.numMap = 0;
% fvalLog.fvalMap = 0;
% save('function estimation log 6.mat', 'fvalLog');
% for wr = [0.5, 1] * 1e-6;
% for wr = [0.1, 0.5, 2.5, 12.5] * 1e-6;
for wr = 2 * 1e-6
%     for vis = 0:0.1:0.1
    for vis = 0:0.1:0.4
        viscosity = vis;
        effortWeight = wr;
        fprintf('\n======================================================================================\n')
        fprintf(['Pattern search with viscosity = %.1f effortWeight = %.1f *1e-6  ', datestr(datetime), '\n'], vis, wr*1e+6);
        options = psoptimset('Display', 'iter', 'TolFun', 0.01, 'TolX', 0.01, 'TolMesh', 0.01);
        % pattern search
        [x,fval,exitflag,output] = patternsearch(@llhmodel, xstart, [], [], [], [], lb, ub, [], options);
        save(sprintf('pattern search result/pattern search result round-8 vis-%.1f wr-%.1f.mat', vis, wr*1e+6), 'x', 'fval', 'exitflag', 'output')
        fprintf(['Optimization result cn = %.2f, sdn = %.2f, fval = %.3f   ', datestr(datetime), '\n'], x(1), x(2), fval);
    end
end

fprintf('Round 8 Optimization Completed\n')

diary OFF
