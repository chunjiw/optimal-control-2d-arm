% solve the problem by global searching

% cn, sdn, pn
% xstart = [0.2, 0.2, 0.05]';
% lb = [0.01; 0.01; 0.01];
% ub = [0.4; 0.4; 0.1];

% cn, sdn
xstart = [0.2, 0.2]';
lb = [.01; .01];
ub = [.4; .4];
problem = createOptimProblem('fmincon', 'lb',lb, 'ub',ub, 'objective',@llhmodel, ...
    'x0',xstart);
gs = GlobalSearch('Display','iter', 'TolFun',0.05, 'TolX',0.01, 'StartPointsToRun','bounds' );
[x,fval,exitflag,output,solutions] = run(gs,problem);