function [ as, bs, phis, stats ] = model( cn, sdn, pn, fig )
%MODEL gives a model depends on constant noise, signal dependent noise and
%planning noise.
%   the model is specified by long and short axis: a, b and ellipse
%   orientation: phi

n = 24;

debugMode = 1;
if debugMode == 1
    clc;
    close all;
    global viscosity;
    viscosity = 0.2;
    fprintf('debug mode in model.m\n')
    fig = 1;
    cn = 0.14;
    sdn = 0.22;
    pn = 0;
    n = 24;
    % for plot
    load('endpointDistributionData.mat');
end

thetas = 2*pi/n * (1:n);
Tmax = 0.49;
Tmin = 0.37;
Ts = sqrt(Tmax^2 * cos(thetas - pi/4).^2 + Tmin^2 * sin(thetas - pi/4).^2);
d = 0.096;      % target distance in meter
wr = 1e-6;
N = 150;
para = struct('planningNoise',pn,'motorNoise',[cn;sdn],'N',N,'T',0,'direc',0,'d',d,'effortCost',wr);

as = nan(1, n);
bs = nan(1, n);
phis = nan(1, n);
% stats = struct;
% fig = figure;
for i = 1:n    
    para.direc = thetas(i);
    para.T = Ts(i);
    if debugMode || nargin > 3
        [as(i), bs(i), phis(i), stat] = endpointstat(para, fig);
    else
        [as(i), bs(i), phis(i), stat] = endpointstat(para);
    end
    stats(i) = stat;
end
stats = [stats(end), stats];

if debugMode
    ratiodata = ratiom;
    ratiosim = [stats.ratio];
    TVdata = TVm * 100;
    TVsim = [stats.TV] * 1000000;
    oriDevsim = [stats.oriDev];
    oriDevdata = direcdevm;

    % plot ratio
    figure;
    polar(trgt_phase,ratiodata,'r-');
    hold on;
    polar(trgt_phase,ratiosim,'b-');
    title(['Aspect Ratio '])
    legend('data', 'sim');
    % plot Total Variance
    figure; 
    polar(trgt_phase,TVdata,'r-');
    hold on;
    polar(trgt_phase,TVsim,'b-');
    title(['Total Variance'])
    legend('data', 'sim');
    % plot Ori Dev
    figure;
    polar(trgt_phase,oriDevdata + 3,'r-');
    hold on;
    polar(trgt_phase,oriDevsim + 3,'b-');
    title(['Ori Dev'])
    legend('data', 'sim');
end


end

