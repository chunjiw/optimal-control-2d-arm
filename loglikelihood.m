function [ llh ] = loglikelihood( as, bs, phis )
%LOGLIKELIHOOD Gives the log likelihood of specified model
%   Detailed explanation goes here

as = as*100;    % convert to centimeter
bs = bs*100;
sbpick = [1 2 3:11];
nBlock = 24;
llhs = nan(1, nBlock);
for iBlock = 1:nBlock
    xy = [];
    for sb = sbpick        
        % load experiment data
        cd ..
        loc = 'data\MT New Experiment\analysis\filtered\';
        filename = sprintf('filtered-sb%02d.mat',sb);
        load([loc,filename])
        cd code
        xEnds = [trajdata{iBlock}.xEnd]';   % centimeter
        yEnds = [trajdata{iBlock}.yEnd]';
        xy_sb = [xEnds-mean(xEnds), yEnds-mean(yEnds)];
        xy = [xy; xy_sb];
        clear xEnds yEnds
    end
    a = as(iBlock);
    b = bs(iBlock);
    phi = phis(iBlock);
    llhs(iBlock) = loglikelihood_sub(a, b, phi, xy);
end

llh = nanmean(llhs);

end

function [ llh ] = loglikelihood_sub( a, b, phi, xy )
    R = [cos(phi), -sin(phi);...
         sin(phi),  cos(phi)];
    n = size(xy, 1);
    llh = - log(a*b) - 1/2/n * trace(xy * R * diag([1/a^2 1/b^2]) * R' * xy');

end

