global viscosity;
viscosity = 0.2;

direc = [pi/4,3*pi/4];

T = [0.2:0.025:2];
% T = [0.4];
Tn=length(T);

cn = 0.14;
sdn = 0.22;
wr = 2*1e-6;
marker = 2;

rate = zeros(Tn,2);

for j = 1:2
    if j == 1
        fprintf('MT-hit curve for right target\n');
    else
        fprintf('MT-hit curve for left target\n');
    end
    for i = 1:Tn
        fprintf('  hit rate when MT = %.02f\n', T(i))
        [rate(i, j)] = hitVSmt(cn,sdn,T(i),direc(j),wr);
    end
end
save('hit_rate_curve', 'T', 'rate', 'wr')

%%
T = T';
figure('color', 'white')
plot(T, rate(:,1), '.', T, rate(:,2),'.', 'linewidth', 1.5);
xlabel('Movement Duration (sec)')
ylabel('Hit Rate (%)')
box off
legend('45 deg', '135 deg')
legend boxoff
