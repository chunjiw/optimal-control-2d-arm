global viscosity;
viscosity = 0.2;

direc = [pi/4,3*pi/4];

T = [0.2:0.025:1];
% T = [0.2,0.225];
Tn=length(T);

cn = 0.14;
sdn = 0.18;
wr = 2*1e-6;
marker = 3;

TVboth = zeros(Tn,2);
TVCN = TVboth;
TVSDN = TVboth;
effort = TVboth;
biasboth = zeros(Tn,2);
biasCN = TVboth;
biasSDN = TVboth;

for j = 1:2
    if j == 1
        fprintf('MT-Variance curve for right target\n');
    else
        fprintf('MT-Variance curve for left target\n');
    end
    for i = 1:Tn
        fprintf('  variance when MT = %.02f\n', T(i))
        for k = 1:10
            [biasboth(i,j,k), TVboth(i,j,k), effort(i,j,k)] = varVSmt(cn,sdn,T(i),direc(j),wr);
            [biasCN(i,j,k),TVCN(i,j,k)] = varVSmt(cn,0,T(i),direc(j),wr);
            [biasSDN(i,j,k),TVSDN(i,j,k)] = varVSmt(0,sdn,T(i),direc(j),wr);
        end
    end
end
biasboth = mean(biasboth,3);
TVboth = mean(TVboth,3);
effort = mean(effort,3);
biasCN = mean(biasCN,3);
TVCN = mean(TVCN,3);
biasSDN = mean(biasSDN,3);          
%%
TVSDN = mean(TVSDN,3);

T = T';
filename = sprintf('MT_var_curve_%i', marker);
save(filename,'effort', 'TVboth', 'biasboth', 'TVCN', 'biasCN', 'TVSDN', 'biasSDN', 'T', 'wr')