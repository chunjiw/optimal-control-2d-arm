function [ llh ] = loglikelihood_synthetic_data( as, bs, phis, stats )
%LOGLIKELIHOOD Gives the log likelihood of specified model
%   Detailed explanation goes here

llhs = nan(1, 32);
for iBlock = 1:32       
    xEnds = stats(iBlock).endx;
    yEnds = stats(iBlock).endy;
    xy = [xEnds-mean(xEnds), yEnds-mean(yEnds)];
    a = as(iBlock);
    b = bs(iBlock);
    phi = phis(iBlock);
    llhs(iBlock) = loglikelihood_sub(a, b, phi, xy);
end
llh = nanmean(llhs);

end



function [ llh ] = loglikelihood_sub( a, b, phi, xy )

R = [cos(phi), -sin(phi);...
     sin(phi),  cos(phi)];
n = size(xy, 1);
llh = - log(a*b) - 1/2/n * trace(xy * R * diag([1/a^2 1/b^2]) * R' * xy');

end

