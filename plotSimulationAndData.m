figure(fig)
set(gcf, 'color', 'white')
set(gca, 'visible', 'off')

%%
thetas = (0:24) * pi/12;
ratiodata = [allRatio(end),allRatio];
ratiodatase = [allRatiose(end), allRatiose] * sqrt(10);
ratiosim = [stats(end).ratio,[stats.ratio]];
TVdata = [allTV(end), allTV] * 100;
TVdatase = [allTVse(end), allTVse] * 100 * sqrt(10);
TVsim = [stats(end).TV, [stats.TV]] * 1000000;
oriDevdata = abs([allDev(end), allDev] * 180/pi);
oriDevdatase = [allDevse(end), allDevse] * 180/pi * sqrt(10);
oriDevsim = abs([stats(end).oriDev, [stats.oriDev]] * 180/pi);

figure;
set(gcf, 'color', 'white')
% plot ratio
subplot(1,3,1)
polar(thetas,ratiodata,'r-');
hold on;
polar(thetas, ratiodata + ratiodatase, 'r:');
polar(thetas, ratiodata - ratiodatase, 'r:');
polar(thetas,ratiosim,'b-');
title(['Aspect Ratio '])
% legend('data', 'sim');
% plot Total Variance
subplot(1,3,2)
polar(thetas,TVdata,'r-');
hold on;
polar(thetas, TVdata + TVdatase, 'r:');
polar(thetas, TVdata - TVdatase, 'r:');
polar(thetas,TVsim,'b-');
title(['Total Variance'])
% legend('data', 'sim');
% plot Ori Dev
subplot(1,3,3)
polarLabels(thetas,oriDevdata + 100,'r-', 0,-100);
hold on;
polarLabels(thetas,oriDevsim + 100,'b-', 0, -100);
polarLabels(thetas,oriDevdata + oriDevdatase + 100,'r:', 0,-100);
polarLabels(thetas,oriDevdata - oriDevdatase + 100,'r:', 0,-100);
title(['Orientation Deviation'])
legend('data', 'sim');