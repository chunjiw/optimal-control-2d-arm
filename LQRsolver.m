function [ uH,sH ] = LQRsolver( state, AkH, ckH, B, R, Qf, sdn )
%LQRSOLVER solves the LQR
% s_(k+1) = A_k * s_k  +  B * u_k  +  c_k
% with cost rate u' * R * u and final cost s_f' * Qf * s_f
% A_k, B_k and c_k are known.
% ver 1.5 - update: apply to any dimension of the state vector
% ver 1.6 - update: extend the solver to deal with signal dependent noise.
%   still open-loop.
% sdn is same length as motor command u (now only works for length 2)
% derivations can be found http://homes.cs.washington.edu/~todorov/papers/TodorovChapter06.pdf

% last check Chunji Wang 5/12/2015

m = size(R,1);          % dimension of command signal
n = length(state);      % dimension of state
T = size(AkH,3);        % number of horizon: k = 1,2,3,...,T.  s_f = s_(T+1)
Qf_ = Qf;
Qf_(n+1,n+1) = 0;       % Qf_ is one dimention more
Ak_H = nan(n+1,n+1,T);
VkH = nan(n+1,n+1,T+1);
VkH(:,:,T+1) = Qf_;
B_ = [B;zeros(1,m)];
B1 = B_;
B1(:,2) = B1(:,2) - B_(:,2);
B2 = B_;
B2(:,1) = B2(:,1) - B_(:,1);
C1 = B1 * sdn(1);       % epsilon
C2 = B2 * sdn(2);
% loop for VkH
for t = T:-1:1
    Ak_ = AkH(:,:,t);
    Ak_(n+1,n+1) = 1;
    ck = ckH(:,t);
    Ak_(:,n+1) = [ck;1];
    Ak_H(:,:,t) = Ak_;      % save for the next loop
    Vk = VkH(:,:,t+1);
    BTV = B_' * Vk;
    CVC = C1'*Vk*C1 + C2'*Vk*C2;
    VkH(:,:,t) = Ak_'*Vk*Ak_ - Ak_'*BTV'*((R + BTV*B_ + CVC)\BTV*Ak_);
end
% loop for uH
uH = nan(m,T);
sH = nan(n,T+1);
state_ = [state;1];
for t = 1:T
    sH(:,t) = state;
    Ak_ = Ak_H(:,:,t);
    Vk = VkH(:,:,t+1);
    BTV = B_' * Vk;
    CVC = C1'*Vk*C1 + C2'*Vk*C2;
    uH(:,t) = -(R + BTV*B_ + CVC)\BTV*Ak_ * state_;
    state_ = Ak_ * state_ + B_ * uH(:,t);
    state = state_(1:n);
end
sH(:,T+1) = state;

end

