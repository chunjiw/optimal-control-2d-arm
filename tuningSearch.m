clc
close all

N = 300;
wr = 1e-8;

cn = .32:.01:.34;
% sdn = .09:.03:.24;
sdn = 0;
icn = length(cn);
isdn = length(sdn);
errormap = nan(icn,isdn);

for i = 1:icn
    for j = 1:isdn
        errormap(i,j) = fitError(N, wr, [cn(i),sdn(j)], 0);
    end
end

save(sprintf('search-N%d-cn_only',N))