close all
clear
load('hit_rate_curve.mat')
idx = T >= 0.4;
Tfit = T(idx);
ratefit = rate(idx, :);
T = T';

myFit = fittype('1/(1 + beta * (t - t_0))', 'independent', 't');
% myFit = fittype('exp(- beta * (t - t_0))', 'independent', 't');
%%
figure('color', 'white')
plot(T, rate(:,1), '.', T, rate(:,2),'.', 'linewidth', 1.5);    hold on;
for j = 1:2
%     [hfit, hgof] = fit(Tfit, ratefit(:,j), hyperbolicFit);
    [efit, egof] = fit(Tfit, ratefit(:,j), myFit);
    xlabel('Movement Duration (sec)');          ylabel('Hit Rate (%)')
    box off;
%     plot(hfit, 'r');
    if j == 1
        plot(efit,'b');
    else
        plot(efit,'r');
    end
end
legend('45 deg', '135 deg'); legend boxoff
xlabel('Movement Duration (sec)'); ylabel('Hit Rate (%)')
xlim([0.2, 2]); ylim([0, 0.9])