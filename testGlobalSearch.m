xstart = [0.2, 0.2]';
cir = @(x) x(1)^2 + x(2)^2;
lb = [0.02, 0.02]';
ub = [0.4, 0.4]';
% problem = createOptimProblem('fmincon','objective',cir, ...
%     'x0',xstart);
% gs = GlobalSearch('Display','iter', 'TolFun',0.05, 'TolX',0.01, 'StartPointsToRun','bounds' );
% [x,fval,exitflag,output,solutions] = run(gs,problem);
options = psoptimset('Display', 'iter');
[x, fval, exitflag, output] = patternsearch(cir, xstart,[],[],[],[], lb, ub, [], options);