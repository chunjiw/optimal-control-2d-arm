function [ as, bs, phis, stats ] = model( cn, sdn, pn, fig )
%MODEL gives a model depends on constant noise, signal dependent noise and
%planning noise.
%   the model is specified by long and short axis: a, b and ellipse
%   orientation: phi

n = 24;
global effortWeight;
wr = effortWeight;
debugMode = 1;

if debugMode == 1
    clc;
%     close all;
    global viscosity;
    viscosity = 0.2;
    wr = 2*1e-6;
    fprintf('debug mode in model.m\n')
    fig = figure;
    cn = 0.14;
    sdn = 0.18;
    pn = 0;
    n = 24;
    % for plot
    load('endpointDistributionData3.mat')
end

thetas = 2*pi/n * (1:n);
Tmax = 0.49;
Tmin = 0.37;
Ts = sqrt(Tmax^2 * cos(thetas - pi/4).^2 + Tmin^2 * sin(thetas - pi/4).^2);
d = 0.096;      % target distance in meter
N = 150;
% N = 10;
para = struct('planningNoise',pn,'motorNoise',[cn;sdn],'N',N,'T',0,'direc',0,'d',d,'effortCost',wr);

as = nan(1, n);
bs = nan(1, n);
phis = nan(1, n);
% stats = struct;
% fig = figure;
for i = 1:n    
    para.direc = thetas(i);
    para.T = Ts(i);
    if debugMode || nargin > 3
        [as(i), bs(i), phis(i), stat] = endpointstat(para, fig);
    else
        [as(i), bs(i), phis(i), stat] = endpointstat(para);
    end
    stats(i) = stat;
end

if debugMode
    save('stats', 'stats');
    figure(fig)
    set(gcf, 'color', 'white')
    set(gca, 'visible', 'off')
    set(gca,'xlim',([-12,12]),'ylim',([-12,12]))

    if ~exist('allRatio','var')
        load('endpointDistributionData3.mat')
    end
    if ~exist('stats','var')
        load('stats.mat');
    end
%     thetas = (0:24) * pi/12;
%     ratiodata = [allRatio(end),allRatio];
%     ratiodatase = [allRatiose(end), allRatiose];
    ratiosim = [stats(end).ratio,[stats.ratio]];
%     TVdata = [allTV(end), allTV];
%     TVdatase = [allTVse(end), allTVse];
    TVsim = [stats(end).TV, [stats.TV]] * 10000;
%     oriDevdata = abs([allDev(end), allDev] * 180/pi);
%     oriDevdatase = [allDevse(end), allDevse] * 180/pi;
    oriDevsim = abs([stats(end).oriDev, [stats.oriDev]] * 180/pi);
    
    figure
    set(gcf, 'color', 'white', 'units','normalized','outerposition',[0 0 1 1])
    % plot ratio
    subplot(1,3,1)
    h = polar0(thetas,ratiodata,'r-');      set(h, 'linewidth', 1.5);
    hold on;
    h = polar0(thetas,ratiosim,'b-');       set(h, 'linewidth', 1.5);
    polar0([thetas; thetas], [ratiodata - ratiodatase; ratiodata + ratiodatase], 'r');
    title(['Aspect Ratio '])
    legend('data', 'sim');
    legend boxoff
    % legend('data', 'sim');
    % plot Total Variance
    subplot(1,3,2)
    h = polar0(thetas,TVdata,'r-');         set(h, 'linewidth', 1.5);
    hold on;
    polar0([thetas; thetas], [TVdata + TVdatase; TVdata - TVdatase], 'r');
    h = polar0(thetas,TVsim,'b-');          set(h, 'linewidth', 1.5);
    title(['Total Variance'])
    % legend('data', 'sim');
    % plot Ori Dev
    subplot(1,3,3)
    h = polar0(thetas,oriDevsim,'b-', 40);  set(h, 'linewidth', 1.5);
    hold on;
    h = polar0(thetas,oriDevdata,'r-', 40); set(h, 'linewidth', 1.5);
    polar0([thetas; thetas],[oriDevdata + oriDevdatase; oriDevdata - oriDevdatase],'r', 40);
    title(['Orientation Deviation'])

end


end

