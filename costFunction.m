function [ errsum ] = costFunction( stats )
%COSTFUNCTION Summary of this function goes here
%   Detailed explanation goes here

% load experiment data
load('BeersDataExp1_32.mat');
direc = direc32;
ratiod = ratio32;
TVd = TotalVariance_mm2/1000000;
n = length(direc);

ratio = nan(1,n);
TV = nan(1,n);
oriDev = nan(1,n);
for i = 1:n
    ratio(i) = stats(i).ratio;
    TV(i) = stats(i).TV;
    oriDev(i) = stats(i).oriDev;
end

% calculate fitting error
err1 = sum(((ratiod - ratio)/0.5).^2);
err2 = sum((oriDev*180/pi/10).^2);
err3 = sum((10^6*(TV - TVd)/20).^2);
errsum = err1 + err2 + err3;
errsum = errsum/n;


end

