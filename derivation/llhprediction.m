% this script demostrates the expectation of likelihood of gaussion data
% point, under gaussion distribution.

% Chunji Wang 3/9/2015

% formula
% llh = - log(sigma*sqrt(2*pi)) - x^2/2/sigma^2
% E(llh) = - log(sigma*sqrt(2*pi)) - 1/2/sigma^2*E(x^2)
%        = - log(sigma*sqrt(2*pi)) - 1/2

sigma = 3;
n = 10000000;
x = sigma*randn(n, 1);
llh = - log(sigma) - x.^2/2/sigma^2;
mllh = mean(llh)
Ellh = - log(sigma) - 1/2
fprintf('the mean value mllh approaches the expectation Ellh\n')

% for 2D gaussion distribution, the formula would be
% formula 2D
% E(llh) = - log(a*b) - 1