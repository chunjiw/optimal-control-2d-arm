function [ traj, effortCost ] = optimalControl2DArm ( motorNoise, direc, T, dist, effortCost )
% ver 2.5 updated: include constant noise cn and signal dependent noise sdn.
% still without delayed feedback.
% 3/20/2014 Chunji
% last check 5/12/2015 Chunji Wang

debugMode = 0;

if debugMode
    clc
%     close all
    fprintf('debug mode in optimalControl2DArm.m\n')
    figure
    visualization = 1;
    motorNoise = [.01;.01];
    T = 0.6;
    direc = 1*pi/4;
    dist = 0.096;
    effortCost = 1e-6;
else
    visualization = 0;
end

% simulation parameters
dt = 0.01;
T = round(T/dt);      % movement time

% cost parameters
wr = effortCost;
wv = 1; 
wp = 1;

% cost matrix
R = wr*[1 0;0 1];

% system synamics
% the rest of synamics parameters are defined in dynamics2DArm
L = [0.3,0.33];
l1 = L(1);
l2 = L(2);

% signal dependent noise
sdn = motorNoise(2);

% task setup
% iniPose = [1.2449;1.4391];      % initial state of arm in theta
iniPose = [pi/4; pi/2];      % initial state of arm in theta
start = forwardKinematics2DArm(iniPose,l1,l2);  % start location of hand
target = [cos(direc); sin(direc)]*dist;
endp = start + target;     % target location
targetPose = inverseKinematics2DArm(endp,l1,l2);              % target state of arm in theta

% solve the optimal control problem
uH = repmat([0;0],1,T);     % the entire motor command series
iniState = [iniPose;zeros(6,1);targetPose;0;0];
Qf = costMatrixQ(wp,wv,[targetPose;0;0],l1,l2);       % the final cost matrix
delta = 1;
nIter = 0;
while delta > 1e-8
    stateH = nan(12,T);
    AkH = nan(12,12,T);
    ckH = nan(12,T);
    state = iniState;
    for t = 1:T
        stateH(:,t) = state;
        [statenext,~,AkH(:,:,t),ckH(:,t),Bk] = dynamics2DArm(state, uH(:,t));
        state = statenext;
    end
    % Solve LQR problem centered around sequence uH
    updated_uH = LQRsolver(iniState,AkH,ckH,Bk,R,Qf,[sdn;sdn]);     % solution doesn't depend on cn
    delta = norm(uH - updated_uH);
    uH = updated_uH;
    nIter = nIter + 1;
    if nIter > 9        
%         disp(['exit with 10 iterations and precition ',num2str(delta)])
        break;        
    end
end

% simulator: simulate trajectories with motor noise
state = iniState;
sH = nan(12,1,T+1);
uH_corrupted = nan(size(uH));
for t = 1:T
    sH(:,t) = state;
    [statenext, uH_corrupted(:,t)] = dynamics2DArm(state, uH(:,t), motorNoise);
    state = statenext;
end
sH(:,:,T+1) = state;

poseH = sH(1:4,:);
traj = nan(4,T+1);
for t = 1:T+1
    traj(:,t) = forwardKinematics2DArm(poseH(:,t),l1,l2);
end
traj = traj - repmat(traj(:,1),1,size(traj,2));
effortCost = trace(uH'*R*uH);

% visualization
if visualization == 1
    subplot(2,2,1);     hold on    
        plot(traj(1,:),traj(2,:),target(1),target(2),'r*')
        title('trajectory')
        axis equal
    subplot(2,2,2);     hold on    
        vx = traj(3,:);
        vy = traj(4,:);
        v = vx.^2 + vy.^2;
        plot(1:T+1,v)
        title('velocity profile')
    subplot(2,2,3);     hold on
        plot(1:T,uH(1,:),1:T,uH(2,:))
        title('motor command')
        legend('shoulder','elbow')    
        % plot(1:T,uH_corrupted(1,:),1:T,uH_corrupted(2,:))
    % plot error vs effort
%         subplot(2,2,4)  
%         cost = [1,trace(uH'*R*uH)/(state'*Qf*state)];
%         bar(cost)
%         title('cost')
%         set(gca,'xTickLabel',{'position & velocity cost','effort cost'})
    % plot joint trajectory
        subplot(2,2,4)
        hold on
        plot(1:T+1,sH(1,:),1:T+1,sH(2,:))
        legend('shoulder','elbow')
        title('joint trajectory')
end

end

function [ x ] = forwardKinematics2DArm( theta,l1,l2 )
%FORWARDKINEMATICS2DARM implements forward kinematics of planer 2-link arm
%   Detailed explanation goes here

theta1 = theta(1);
theta2 = theta(2);
theta12 = theta1 + theta2;
l1c1 = l1 * cos(theta1);
l1s1 = l1 * sin(theta1);
l2c12 = l2 * cos(theta12);
l2s12 = l2 * sin(theta12);
x = zeros(2,1);
x(1) = l1c1 + l2c12;
x(2) = l1s1 + l2s12;

if length(theta) == 4    
    d1 = theta(3);
    d2 = theta(4);
    d12 = d1 + d2;
    x(3) = - l1s1 * d1 - l2s12 * d12;
    x(4) =   l1c1 * d1 + l2c12 * d12;
end

end

function [ theta ] = inverseKinematics2DArm( xy,l1,l2 )
%INVERSEKINEMATICS2DARM implements inverseKinematics of planer 2-link arm
% found http://www.eng.utah.edu/~cs5310/chapter5.pdf
%   Detailed explanation goes here

x = xy(1);
y = xy(2);
x2 = x^2;
y2 = y^2;
lp2 = (l1 + l2)^2;
lm2 = (l1 - l2)^2;

theta2 = 2 * atan(sqrt( (lp2-x2-y2)/(x2+y2-lm2) ));
phi = atan2(y,x);
psi = atan2( l2*sin(theta2), l1+l2*cos(theta2) );
theta1 = phi - psi;

theta = [theta1;theta2];

end

function [ Q ] = costMatrixQ( wp,wv,theta,l1,l2 )
%COSTMATRIXQ gives the final cost matrix Q 12*12 around target pose theta
%   Detailed explanation goes here
% theta is the position and velocity of joint angle

Qp = diag([wp,wp,wv,wv]);

theta1 = theta(1);
theta2 = theta(2);
theta12 = theta1 + theta2;
theta1dot = theta(3);
theta2dot = theta(4);
theta12dot = theta1dot + theta2dot;
l1s1 = l1*sin(theta1);
l1c1 = l1*cos(theta1);
l1c1dot1 = l1c1 * theta1dot;
l1s1dot1 = l1s1 * theta1dot;
l2s12 = l2*sin(theta12);
l2c12 = l2*cos(theta12);
l2c12dot12 = l2c12 * theta12dot;
l2s12dot12 = l2s12 * theta12dot;  
          
% Jacobian 4*4
J(1:2,1:2) = [-l1s1-l2s12, -l2s12
               l1c1+l2c12,  l2c12  ];
J(3:4,1:4) = [-l1c1dot1-l2c12dot12, -l2c12dot12, -l1s1-l2s12, -l2s12
              -l1s1dot1-l2s12dot12, -l2s12dot12,  l1c1+l2c12,  l2c12];        % Jacobian

Delta = [eye(4),zeros(4),-eye(4)];

Q = Delta' * J' * Qp * J * Delta;

end

