
% generate synthetic data
[~,~,~,stats] = model(0.2, 0.1, 0.05);

%% search maximum likelihood estimation
A = [- eye(3); eye(3)];
b = [-0.01 -.01 -0.01 .4 .4 .1 ]';
noise = fmincon(@(x)llhmodel_synthetic_data(x, stats), [0.2 0.2 0.05], A, b);

