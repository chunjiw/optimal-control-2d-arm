function [ r2, r1, ellipdirec, stat ] = endpointstat( para, fig )
%ENDPOINTSETGEO Gives the statistics of endpoint distribution, and plot on
%fig.
% questions remained: movement variability (mv) should be variance or
% standard deviation?
% Chunji Wang 3/20/2014

debugMode = 0;

% disp('called')

if debugMode
    % input_args: para
    close all
    fprintf('debug mode in endpointstat.m\n')
    fig = figure;
    para = struct('planningNoise', 0, 'motorNoise',[0.173;0.106],...
         'N',150,'T',0.2,'direc',3*pi/4,'d',0.096,'effortCost',1e-6);
    %     'N',100,'T',0.6,'direc',pi/4,'d',0.096,'effortCost',1e-6);
end

motorNoise = para.motorNoise;
direc = para.direc;
T = para.T;
N = para.N;
wr = para.effortCost;
pn = para.planningNoise;
dist = para.d;

% sampling
effortCostN = nan(N,1);
[traj, temp] = optimalControl2DArm(motorNoise, direc, T, dist, wr);
effortCostN(1) = temp;
tn = size(traj,2);
trajx = nan(N,tn);
trajx(1,:) = traj(1,:);
trajy = trajx;
trajy(1,:) = traj(2,:);
for i = 2:N
    [traj, temp] = optimalControl2DArm(motorNoise, direc, T, dist * ( 1 + pn*randn), wr);
    effortCostN(i) = temp;
    trajx(i,:) = traj(1,:);
    trajy(i,:) = traj(2,:);
end
endx = trajx(:,end);
endy = trajy(:,end);
meanx = mean(endx);
meany = mean(endy);
ellipcenter = [meanx;meany];
C = cov(endx,endy);
[V,D] = eig(C);
TV = trace(C);      % total variance or variable error

effortCost = sum(effortCostN) / N;

targetxy = dist*[cos(direc);sin(direc)];
bias = (meanx-targetxy(1))^2 + (meany-targetxy(2))^2;

% movement variability along movement
mv = var(trajx) + var(trajy);
mv = mv' * 10^6;        % msm^2

% The first eigenvalue is smaller. (MATLAB R2011a)
r = diag(D);
r1 = r(1);
r2 = r(2);
r1 = sqrt(r1);
r2 = sqrt(r2);
ratio = r2/r1;

% The orientation of the ellipse, [0,pi]
ellipdirec = atan(V(2,2)/V(1,2));
if ellipdirec < 0
    ellipdirec = ellipdirec+pi;
end

direch = mod(direc,pi);
oriDev = ellipdirec - direch;
if oriDev > pi/2
    oriDev = pi - oriDev;
elseif oriDev < -pi/2
    oriDev = pi + oriDev;
end

if ratio < 2
    oriDev = oriDev*(ratio-1) * 2/3;
end

stat = struct('endx',endx,'endy',endy,'r1',r1,'r2',r2,'bias',bias,'TV',TV,'effortCost',effortCost,'ratio',ratio,'oriDev',oriDev,'C',C);

% visuallization
if nargin > 1 || debugMode
    trajx = mean(trajx);
    trajy = mean(trajy);
    figure(fig);
    hold on;
    r1 = r1 * 2;
    r2 = r2 * 2;
    plot(r1*[-V(1,1) V(1,1)] + ellipcenter(1), r1*[-V(2,1) V(2,1)] + ellipcenter(2),'g');
    plot(r2*[-V(1,2) V(1,2)] + ellipcenter(1), r2*[-V(2,2) V(2,2)] + ellipcenter(2),'g');
    plot(trajx,trajy);
%     plot(targetxy(1),targetxy(2),'r*');
%     plot([0,ellipcenter(1)],[0,ellipcenter(2)],'r:')
    drawellip(r2,r1,ellipcenter,ellipdirec,'r');
    axis equal
    if debugMode
        figure
        subplot(2,2,1)
        plot(mv)
        subplot(2,2,2)
        plot(gradient(mv))
    end
end

end

function [ A ] = drawellip( a,b,center,theta,options )
%DRAWELLIP draws an ellipse
%   [ A ] = drawellip( a,b,center,theta )

if nargin<5
    options='b';
end

if nargin<4
    theta=0;
end

if nargin<3
    center=[0,0];
end

xp1=linspace(-a,a);
xp2=linspace(a,-a);
yp1=b*sqrt(1-xp1.^2/a^2);
yp2=-b*sqrt(1-xp2.^2/a^2);

% Rotaion matrix
R=[cos(theta) -sin(theta);sin(theta) cos(theta)];
xy=R*[[xp1,xp2];[yp1,yp2]];
xy=xy';
x=xy(:,1)+center(1);
y=xy(:,2)+center(2);
A=pi*a*b;
plot(x,y,options, 'linewidth',2);
% axis equal
end




