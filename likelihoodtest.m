% survey on ellipse fit and likelihood estimation
close all
clc

%% generate artificial data
a = 2;
b = 1;
n = 150;
x = a * randn(n,1);
y = b * randn(n,1);
xy = [x,y];
phi = pi/6;
R = [cos(phi), -sin(phi); sin(phi), cos(phi)];
xy = xy * R';
x = xy(:,1);
y = xy(:,2);

alpha = 0.05;       % quantile
c = sqrt( -2*log(alpha) );
th = linspace(0, 2*pi, n);
xcurve = c*a * cos(th);
ycurve = c*b * sin(th);
% sum(((x./a).^2 + (y./b).^2) > c^2)  % test quantile
plot(x,y,'.', xcurve, ycurve)
axis equal

%% analyze artificial data
C = cov([x, y]);
[V,D] = eig(C);
phi = atan2(V(2,2),V(1,2));
R = [cos(phi), -sin(phi); sin(phi), cos(phi)];
xcurve = c * sqrt(D(2,2)) * cos(th);
ycurve = c * sqrt(D(1,1)) * sin(th);
xycurve = [xcurve; ycurve]' * R';

hold on
plot(xycurve(:,1), xycurve(:,2))
hold off

%% likelihood calculation
% 1. model selection: maximum likelihood estimation MLE
% model parameters: ma, mb, mphi
% log likelihood formula:
% -log(ma*mb) - 1/2/n * trace(xy * mR' * diag([1/ma^2, 1/mb^2]) * mR * xy');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % test likelihood formular                                              % 
% ma = 4;                                                                 %
% mb = 1;                                                                 %
% mphi = pi/6;                                                            %
% mR = [cos(mphi), sin(mphi); -sin(mphi), cos(mphi)];                     %
% xx = -6:.02:6;                                                          %  
% yy = -3:.01:3;                                                          %
% for i = 1:length(xx)                                                    %
%     for j = 1:length(yy)                                                % 
%         llh(i,j) = -log(ma*mb) - 1/2 * diag([xx(i),yy(j)] * mR' * ...   %
% diag([1/ma^2, 1/mb^2]) * mR * [xx(i);yy(j)]);                           %
%     end                                                                 %
% end                                                                     %
% contour(xx,yy,llh)                                                      %
% axis equal                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% likelihood sensibility on orientation
ma = a;
mb = b;
phis = linspace(0, pi*2, 100);
llh = nan(1,100);
for i = 1:100
    mphi = phis(i);
    mR = [cos(mphi), sin(mphi); -sin(mphi), cos(mphi)];
    llh(i) = -log(ma*mb) - 1/2/n * trace(xy * mR' * diag([1/ma^2, 1/mb^2]) * mR * xy');
end
figure
plot(phis,llh)
% likelihood sensibility on axis
mas = linspace(a-1, a+1, 100);
mb = b;
mphi = 0;
mR = [cos(mphi), sin(mphi); -sin(mphi), cos(mphi)];
llh = nan(1,100);
for i = 1:100
    ma = mas(i);    
    llh(i) = -log(ma*mb) - 1/2/n * trace(xy * mR' * diag([1/ma^2, 1/mb^2]) * mR * xy');
end
figure
plot(mas,llh)
[~, id] = max(llh);
title(num2str(mas(id)))
%% 2. data selection: select data which best fit the model
% conclusion: this is wrong. the data which centers at the peak of the
% model pdf wins.
% specify model
ma = 2;
mb = 1;
mphi = 0;
mR = [cos(mphi), sin(mphi); -sin(mphi), cos(mphi)];
% generate data
as = linspace(0.1,3, 100);
b = 1;
% phis = linspace(0, 2*pi, 100);
phi = 0;
n = 150;
for i = 1:100
%     phi = phis(i);
    a = as(i);
    R = [cos(phi), -sin(phi); sin(phi), cos(phi)];
    x = a * randn(n,1);
    y = b * randn(n,1);
    xy = [x,y] * R';
    llh(i) = -log(ma*mb) - 1/2/n * trace(xy * mR' * diag([1/ma^2, 1/mb^2]) * mR * xy');
end
figure
plot(phis, llh)