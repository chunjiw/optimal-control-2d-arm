%% load
load('MT_var_curve_3.mat' );

%% plot curves
close all
figure
set(gcf, 'color', 'white')
for j = 1:2
    subplot(1,2,3-j)
    plot(T, TVboth(:,j),  T, TVboth(:,j) + effort(:,j), 'linewidth', 1.5)
    hold on
    plot(T, TVCN(:,j), '--k')
    plot(T, TVSDN(:,j), '-.k')
    ylabel('Variable Error (cm^2)')
    xlabel('Movement Duration (sec)')
    ylim([0,4])
    box off
    if j == 1,   title('45 deg')
    else         title('135 deg')
    end
end
legend('error','cost','CN only', 'SDN only')
% legend('error','cost')
legend boxoff