close all

ratiodata = ratiom;
ratiosim = [stats.ratio];
TVdata = TVm * 100;
TVsim = [stats.TV] * 1000000;
oriDevsim = [stats.oriDev];
oriDevdata = direcdevm;

% plot ratio
figure; hold on;
polar(trgt_phase,ratiodata,'r-');
polar(thetas,ratiosim,'b-');
title(['Aspect Ratio '])
legend('data', 'sim');
% plot Total Variance
figure; hold on;
polar(trgt_phase,TVdata,'r-');
polar(thetas,TVsim,'b-');
title(['Total Variance'])
legend('data', 'sim');
% plot Ori Dev
figure; hold on;
polar(trgt_phase,oriDevdata + 3,'r-');
polar(thetas,oriDevsim + 3,'b-');
title(['Ori Dev'])
legend('data', 'sim');
