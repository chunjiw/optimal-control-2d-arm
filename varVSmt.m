function [ bias, TV, effort ] = varVSmt( cn,sdn,T,direc,wr )
%MTSIMU Calculate the end point variability as a function of movement time T
%   Detailed explanation goes here

% sigma=0.03;
% sigmau=0.12;
% N=50;

d = 0.074;

para = struct('planningNoise', 0, 'motorNoise',[cn; sdn],...
         'N',150,'T',T,'direc',direc,'d',d,'effortCost',wr);
[~, ~, ~, stat] = endpointstat(para);

% endpointvar = stat.TV + stat.bias;
bias = stat.bias * 10000;
TV = stat.TV * 10000;
effort = stat.effortCost * 10000;

end

